package main

// Invite link: https://discord.com/oauth2/authorize?client_id=1210768814011191316&scope=bot&permissions=134144

import (
	"context"
	"fmt"
	"os"
	"sort"
	"time"

	"cloud.google.com/go/datastore"
	"github.com/bwmarrin/discordgo"
	"github.com/cenkalti/backoff/v4"
	"github.com/go-co-op/gocron/v2"
	"github.com/mmcdole/gofeed"
)

var actionedGUIDs = make(map[string]bool)

var discordBotToken string = os.Getenv("DISCORD_BOT_TOKEN")
var pingRoleID string = os.Getenv("PING_ROLE_ID")
var targetChannelID string = os.Getenv("TARGET_CHANNEL_ID")
var gCloudProjectID string = os.Getenv("GCLOUD_PROJECT_ID")
var feedURL string = os.Getenv("FEED_URL")
var devEnvironment bool = os.Getenv("BOT_ENV") == "dev"

const interval time.Duration = time.Second * 60
const cutoff time.Duration = time.Hour * 24 * 30 * 6 // Six months

const rssPostGUIDKind = "rssPostGUID"

func loadFromDatabase() error {
	ctx := context.Background()
	client, err := datastore.NewClientWithDatabase(ctx, gCloudProjectID, "rss-notifier")
	if err != nil {
		return fmt.Errorf("failed to create client: %v", err)
	}
	defer client.Close()

	query := datastore.NewQuery(rssPostGUIDKind).KeysOnly()

	keys, err := client.GetAll(ctx, query, nil)
	if err != nil {
		return fmt.Errorf("failed to read values from database: %v", err)
	}
	for _, key := range keys {
		guid := key.Name
		actionedGUIDs[guid] = true
	}
	fmt.Printf("loaded %v already-actioned posts\n", len(keys))

	return nil
}

func recordToDatabase(guid string) error {
	ctx := context.Background()
	client, err := datastore.NewClientWithDatabase(ctx, gCloudProjectID, "rss-notifier")
	if err != nil {
		return fmt.Errorf("failed to create client: %v", err)
	}
	defer client.Close()

	key, err := client.Put(ctx, datastore.NameKey(rssPostGUIDKind, guid, nil), &struct{}{})
	if err != nil {
		return fmt.Errorf("failed to put new guid in database: %v", err)
	}

	fmt.Printf("recorded post %s to database\n", key.Name)

	return nil
}

func isNew(item *gofeed.Item) bool {
	return item.PublishedParsed.After(time.Now().Add(-cutoff))
}

func getNewPost() (items []*gofeed.Item, err error) {
	rssParser := gofeed.NewParser()
	feed, err := rssParser.ParseURL(feedURL)
	if err != nil {
		return nil, fmt.Errorf("error getting/parsing feed: %v", err)
	}
	sort.Sort(sort.Reverse(feed))
	updateTime := feed.UpdatedParsed
	if updateTime.After(time.Now().Add(-cutoff)) {
		newItems := make([]*gofeed.Item, 0)
		for _, item := range feed.Items {
			itemIsNew := isNew(item)
			if itemIsNew && !actionedGUIDs[item.GUID] {
				newItems = append([]*gofeed.Item{item}, newItems...)
			} else if !itemIsNew {
				break
			}
		}
		return newItems, nil
	}
	return nil, nil
}

func sendMessage(session *discordgo.Session, item *gofeed.Item) (*discordgo.Message, error) {
	announcementString := fmt.Sprintf("<@&%s>\n**%s**\n%s", pingRoleID, item.Title, item.Link)
	message, err := session.ChannelMessageSend(targetChannelID, announcementString)
	if err != nil {
		return nil, err
	}
	fmt.Printf("message: %v\n", message.Content)
	return message, nil
}

func unpinMyOldMessages(session *discordgo.Session) error {
	allPinned, err := session.ChannelMessagesPinned(targetChannelID)
	if err != nil {
		return fmt.Errorf("could not list pinned messages: %v", err)
	}

	me, err := session.User("@me")
	if err != nil {
		return fmt.Errorf("could not find my ID: %v", err)
	}

	for _, message := range allPinned {
		if message.Author.ID == me.ID {
			err = session.ChannelMessageUnpin(targetChannelID, message.ID)
			if err != nil {
				return fmt.Errorf("could not unpin message with id %v: %v", message.ID, err)
			}
			fmt.Println("unpinned an old message")
		}
	}

	return nil
}

func connectToDiscord() (*discordgo.Session, error) {
	session, err := discordgo.New("Bot " + discordBotToken)
	if err != nil {
		return nil, err
	}

	session.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		fmt.Println("session ready")
	})

	err = session.Open()
	if err != nil {
		return nil, err
	}
	return session, nil
}

func recordNotification(guid string) error {
	actionedGUIDs[guid] = true
	err := recordToDatabase(guid)
	return err
}

func checkAndNotify() {
	if devEnvironment {
		fmt.Printf("%s checking for updates...\n", time.Now()) // TODO disable
	}

	items, err := getNewPost()
	if err != nil {
		fmt.Println(err)
	}
	if len(items) > 0 {
		fmt.Println("updated! notifying...")

		session, err := connectToDiscord()
		if err != nil {
			fmt.Printf("error connecting to discord: %v", err)
			return
		}
		defer session.Close()

		for _, item := range items {

			err := unpinMyOldMessages(session)
			if err != nil {
				fmt.Printf("warning: could not unpin last message: %v", err)
			}

			message, err := sendMessage(session, item)
			if err != nil {
				fmt.Printf("error sending message to discord: %v", err)
				return
			}

			err = session.ChannelMessagePin(targetChannelID, message.ID)
			if err != nil {
				fmt.Printf("warning: could not pin message to discord: %v", err)
			} else {
				fmt.Println("pinned the notification message")
			}

			record := func() error { return recordNotification(item.GUID) }
			log := func(err error, wait time.Duration) {
				fmt.Printf("failed to update database after waiting %s: %v", wait, err)
			}
			err = backoff.RetryNotify(record, backoff.NewExponentialBackOff(), log)
			if err != nil {
				fmt.Printf("failed all retries to update database, exiting: %v", err)
				session.Close()
				os.Exit(1)
			}

			//lint:ignore SA4004 Limit to one new post per interval (for now, at least)
			break
		}
	}
}

func main() {
	err := loadFromDatabase()
	if err != nil {
		fmt.Printf("error loading from db: %v", err)
		return
	}

	s, err := gocron.NewScheduler()
	if err != nil {
		fmt.Printf("error creating scheduler: %v", err)
		return
	}

	// every interval, run checkAndNotify unless it's already running
	_, err = s.NewJob(
		gocron.DurationJob(interval),
		gocron.NewTask(checkAndNotify),
		gocron.WithSingletonMode(gocron.LimitModeReschedule),
	)
	if err != nil {
		fmt.Printf("error creating job: %v", err)
		return
	}
	s.Start()

	// block forever
	select {}
}
